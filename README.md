# CI for Java

## Images

### jre

Contains java runtime.

### jdk

Contains JDK and maven.

## Build locally

Run as unprivileged user, produces `localhost/jre` and `localhost/jdk`.

```sh
buildah unshare ./build.sh
```
