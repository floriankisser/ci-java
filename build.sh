#!/usr/bin/env sh

set -ex
. ./env.sh

base=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/ci-base/base
toolbox=$CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE/ci-base/toolbox

arch=$1

case $arch in
  ""|amd64)
    rpm_arch=x86_64
    ;;
  arm64)
    rpm_arch=aarch64
    ;;
  *)
    >&2 echo "Unknown architecture: $arch"
    exit 1
    ;;
esac

dnf_cache=$(pwd)/build/cache/dnf
java_version=21
maven_version=3.9.9
maven_name=apache-maven-${maven_version}
maven_tar=${maven_name}-bin.tar.gz
maven_url=https://dlcdn.apache.org/maven/maven-3/${maven_version}/binaries/${maven_tar}

mkdir -p "$dnf_cache"

ctr=$(buildah from --override-arch=$arch "$base")
mnt=$(buildah mount "$ctr")
tools=$(buildah from -v "$mnt":/centos_root -v /dev/null:/centos_root/dev/null -v "$dnf_cache":/centos_root/var/cache/dnf:z "$toolbox")

buildah run "$tools" -- dnf -y --installroot=/centos_root --forcearch=$rpm_arch install \
  java-${java_version}-openjdk-headless

buildah rm "$tools"

buildah config --label "name=jre" "$ctr"
buildah config --label "commit=$CI_COMMIT_SHA" "$ctr"

ctr=$(buildah from "$(buildah commit --rm "$ctr" jre)")
mnt=$(buildah mount "$ctr")
tools=$(buildah from -v "$mnt":/centos_root -v /dev/null:/centos_root/dev/null -v "$dnf_cache":/centos_root/var/cache/dnf:z "$toolbox")

buildah run "$tools" -- dnf -y --installroot=/centos_root --forcearch=$rpm_arch install \
  java-${java_version}-openjdk-devel

buildah run "$tools" -- wget "$maven_url"
buildah add --from "$tools" "$ctr" "$maven_tar" /usr/local/
buildah run "$ctr" -- alternatives --install /usr/bin/mvn mvn /usr/local/${maven_name}/bin/mvn 1

buildah rm "$tools"

buildah config --label "name=jdk" "$ctr"
buildah config --label "commit=$CI_COMMIT_SHA" "$ctr"

buildah commit --rm "$ctr" jdk
